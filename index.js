/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function personalInfo(){
		let fullName = prompt('Enter your full name:');
		let age = prompt('Enter your age:');
		let location = prompt('Enter your location:');

		console.log('Hi ' + fullName);
		console.log(fullName + '\'s age is ' + age);
		console.log(fullName + ' is located at ' + location);

	};
	personalInfo();

/*

	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	function myTopFiveBand(){
		let topFiveBand = ['Chocolate Factory', 'Kamikazee', 'Eagles', 'Air Supply', 'Parokya Ni Edgar'];
		console.log(topFiveBand);
	};
	myTopFiveBand();
	

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function myTopFiveMovies(){
		let faveMovie1 = 'The Purge: Anarchy';
		let movieRate1 = '57%';
		console.log('1. ' + faveMovie1 );
		console.log("Tomatometer for Anarchy: " + movieRate1);

		let faveMovie2 = 'Transformer';
		let movieRate2 = '100%';
		console.log('1. ' + faveMovie2 );
		console.log("Tomatometer for Transformer: " + movieRate2);

		let faveMovie3 = 'CopShop';
		let movieRate3 = '81%';
		console.log('3. ' + faveMovie3 );
		console.log("Tomatometer for CopShop: " + movieRate3);

		let faveMovie4 = 'John Wick';
		let movieRate4 = '86%';
		console.log('4. ' + faveMovie4 );
		console.log("Tomatometer for John Wick: " + movieRate4);

		let faveMovie5 = 'BuyBust';
		let movieRate5 = '83%';
		console.log('5. ' + faveMovie5 );
		console.log("Tomatometer for BuyBust: " + movieRate5);
	};
	myTopFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


    function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);
};
printFriends();